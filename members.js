const members = [
    {
        id: 1,
        name: "John",
        email: "sth@fmail.com",
        home: "EN",
        active: "yes"
    },
    {
        id: 2,
        name: "hello",
        email: "hello@yahoo.com",
        home: "US",
        active: "no"
    },
    {
        id: 3,
        name: "blah",
        email: "greatgod@god.org",
        home: "VA",
        active: "yes"
    },
    {
        id: 5,
        name: "Bruh",
        home: "JJ",
        email: "goodboi@cmu.edu",
        active: "yes"
    },
]
module.exports = members