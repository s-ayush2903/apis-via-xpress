const express = require('express');
const router = express.Router();
// const members = require('../../members');
const member = require('../../models/member');
const uuid = require('uuid');
const mngz = require('mongoose');

// list all members
router.get('/', paginate_results(member), (req, res) => res.json(res.paginate_results))

// get single member
router.get('/:name', (req, res) => {
    const found = members.some(member => member.name === req.params.name);
    if (found) {
        res.json(members.filter(member => member.name === req.params.name));
    } else {
        res.status(400).json({ msg: `member with name ${req.params.name} not found` });
    }
})

// create a new member
router.post('/', (req, res) => {
    const new_member = {
        id: uuid.v4(),
        name: req.body.name,
        email: req.body.email,
        status: 'active'
    }
    if (!new_member.email || !new_member.name) {
        return res.json({ msg: 'user\'s name or email is missing' });
    }
    members.push(new_member);
    console.debug(members);
    // res.json({
    //     msg: `successfully created user with name: ${new_member.name} and email: ${new_member.email}, generated id: ${new_member.id}`
    // }
    // );
    // res.json(members);
    res.redirect('/');
});

// update an existing member
router.put('/:id', (req, res) => {
    const r_member = req.body;
    const found = members.some(_ => _.id === parseInt(req.params.id));
    if (found) {
        members.forEach(_ => {
            console.log(_.id);
            if (_.id === parseInt(req.params.id)) {
                _.name = r_member.name ? r_member.name : _.name;
                _.email = r_member.email ? r_member.email : _.email;
                res.json({ msg: 'member updated', member: _ });
            }
        });
    } else {
        res.status(400).json({ msg: `no member with id ${req.params.id} found` });
    }
    console.log(found);
});

// delete member
router.delete('/:id', (req, res) => {
    const found = members.filter(_ => _.id === parseInt(req.params.id));
    console.log(found);
    if (found) {
        res.json({ msg: 'member deleted', mems: members.filter(_ => _.id != parseInt(req.params.id)) });
    } else {
        res.status(400).json({ msg: `no member with id ${id} found in db` });
    }
})

const LIM = 3;
function paginate_results(model) {
    return async (req, res, next) => {
        const page = parseInt(req.query.page);
        console.debug(req.query);
        const lim = LIM || req.query.limit;
        const start_idx = (page - 1) * lim;
        const end_idx = (page) * lim;
        const results = {};
        if (end_idx < await model.countDocuments().exec()) {
            results.next = {
                page: page + 1,
                items_per_page: lim
            };
        }
        if (start_idx > 0) {
            results.prev = {
                page: page - 1,
                items_per_page: lim
            };
        }
        try {
            results.results = await model.find().limit(lim).skip(start_idx).exec();
            res.paginate_results = results;
            next();
        } catch (e) {
            res.status(500).json({ error: e });
        }
    }
}

module.exports = router;