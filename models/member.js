const mongoose = require('mongoose');
const member_schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    id: {
        type: String,
        required: true
    },
    home: {
        type: String,
        required: false
    },
    active: {
        type: Boolean,
        required: false
    }
});

module.exports = mongoose.model('Member', member_schema);
