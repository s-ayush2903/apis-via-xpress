const express = require('express');

const path = require('path');
const xpress_handlebars = require('express-handlebars');
const members_arr = require('./members');
const _member = require('./models/member');
const logger = require('./middleware/logger');
// const { default: mongoose } = require('mongoose');
const mongoose = require('mongoose');

const app = express();
mongoose.connect('mongodb://localhost:27017/pagination');
const db = mongoose.connection;
db.once('open', async () => {
    if (await _member.countDocuments().exec() > 0) return;

    Promise.all([
        _member.create({ name: "Alex", id: "69", email: "alex@fmail.com" }),
        _member.create({ name: "David", id: "9", email: "dave@gmail.com" }),
        _member.create({ name: "Brad", id: "6", email: "brad@fmail.com" }),
        _member.create({ name: "Mike", id: "19", email: "mike@gmail.com" }),
        _member.create({ name: "Mitch", id: "61", email: "mitch@fmail.com" }),
        _member.create({ name: "Jake", id: "64", email: "jake@fmail.com" }),
        _member.create({ name: "Manuel", id: "59", email: "manuel@fmail.com" }),
        _member.create({ name: "Florina", id: "89", email: "florian@fmail.com" }),
        _member.create({ name: "Chris", id: "28", email: "chris@fmail.com" })
    ]).then(() => console.log('Added Users'));
});

// db.once('open', async() => {
//     if ((await _member.countDocuments().size()) > 0) return;
//     else {
//         Promise.all([
//         ]).then(() => console.debug('Created members'));
//     }
// });

// app.use(logger)

// handlebars middleware
app.engine('handlebars', xpress_handlebars.engine({ defaultLayout: "main" }));
app.set('view engine', 'handlebars');

// body parser middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false }));

// app.get('/');
app.get('/', (req, res) => res.render('index', { title: "Master title", members: members_arr }));

// set static dir
app.use(express.static(path.join(__dirname, 'public')));

// members API
app.use('/api/members/', require('./routes/api/members'));

// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public'))
// })

const PORT = process.env.PORT || 6969;

app.listen(PORT, () => console.log(`server started on port: ${PORT}`));